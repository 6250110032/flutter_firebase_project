import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter_firebase_auth/login.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Remove the debug banner
      debugShowCheckedModeBanner: false,
      title: 'Flutter firebase auth ',
      home: AnimatedSplashScreen(
          splash: Icons.audiotrack_outlined,
          duration: 3000,
          splashTransition: SplashTransition.scaleTransition,
          backgroundColor: Colors.indigoAccent,
          nextScreen: Login()),
      //   Center(
      //  child: Column(
      //  mainAxisAlignment: MainAxisAlignment.center,
      //  children: [
      //  Container(height: 100, width:100,color: Colors.blue,),
      //  Container(
      //  child: Text(
      // 'Splash Screen',
      // style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
      // ),
      // ),
      // ],
      //),
      //),
    );
  }
}
