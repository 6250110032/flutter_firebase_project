import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_firebase_auth/about.dart';
import 'package:flutter_firebase_auth/chart.dart';
import 'package:flutter_firebase_auth/user.dart';
import 'package:google_fonts/google_fonts.dart';

class Home extends StatefulWidget {
  const Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  FirebaseAuth _auth = FirebaseAuth.instance;
  get user => _auth.currentUser;
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _priceController = TextEditingController();
  final TextEditingController _sizeController = TextEditingController();
  final TextEditingController _modelController = TextEditingController();



  // Create a CollectionReference called _products that references the firestore collection
  final CollectionReference _products =
  FirebaseFirestore.instance.collection('products');

  // This function is triggered when the floatting button or one of the edit buttons is pressed
  // Adding a product if no documentSnapshot is passed
  // If documentSnapshot != null then update an existing product
  Future<void> _createOrUpdate([DocumentSnapshot documentSnapshot]) async {
    String action = 'create';
    if (documentSnapshot != null) {
      action = 'update';
      _nameController.text = documentSnapshot['name'];
      _priceController.text = documentSnapshot['price'].toString();
      _sizeController.text = documentSnapshot['size'].toString();
      _modelController.text = documentSnapshot['model'].toString();
    }

    await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext ctx) {
          return Padding(
            padding: EdgeInsets.only(
                top: 20,
                left: 20,
                right: 20,
                // prevent the soft keyboard from covering text fields
                bottom: MediaQuery.of(ctx).viewInsets.bottom + 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextField(
                  controller: _nameController,
                  decoration: const InputDecoration(labelText: 'Name'),
                ),
                TextField(
                  keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
                  controller: _priceController,
                  decoration: const InputDecoration(
                    labelText: 'Price',
                  ),
                ),
                TextField(
                  controller: _sizeController,
                  decoration: const InputDecoration(labelText: 'size'),
                ),
                TextField(
                  controller: _modelController,
                  decoration: const InputDecoration(labelText: 'model'),
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  child: Text(action == 'create' ? 'Create' : 'Update'),
                  onPressed: () async {
                    final String name = _nameController.text;
                    final double price = double.tryParse(_priceController.text);
                    final String size = _sizeController.text;
                    final String model = _modelController.text;
                    if (name != null && price != null && size != null && model != null) {
                      if (action == 'create') {
                        // Persist a new product to Firestore
                        await _products
                            .add({"name": name, "price": price,"size": size,"model": model})
                            .then((value) => print("Product Added"))
                            .catchError((error) =>
                            print("Failed to add product: $error"));
                      }

                      if (action == 'update') {
                        // Update the product
                        await _products
                            .doc(documentSnapshot.id)
                            .update({"name": name, "price": price,"size": size,"model": model})
                            .then((value) => print("Product Updated"))
                            .catchError((error) =>
                            print("Failed to update product: $error"));
                      }

                      // Clear the text fields
                      _nameController.text = '';
                      _priceController.text = '';
                      _sizeController.text = '';
                      _modelController.text = '';

                      // Hide the bottom sheet
                      Navigator.of(context).pop();
                    }
                  },
                )
              ],
            ),
          );
        });
  }

  // Deleting a product by id
  Future<void> _deleteProduct(String productId) async {
    await _products
        .doc(productId)
        .delete()
        .then((value) => print("Product Deleted"))
        .catchError((error) => print("Failed to delete product: $error"));

    // Show a snackbar
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('You have successfully deleted a product')));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle:
        SystemUiOverlayStyle(statusBarColor: Colors.indigoAccent),
        backgroundColor: Colors.indigo,
        title: const Text('Firebase'),
      ),
      // Using StreamBuilder to display all products from Firestore in real-time
      body: StreamBuilder(
        stream: _products.snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> streamSnapshot) {
          if (streamSnapshot.hasData) {
            return ListView.builder(
              itemCount: streamSnapshot.data.docs.length,
              itemBuilder: (context, index) {
                final DocumentSnapshot documentSnapshot =
                streamSnapshot.data.docs[index];
                return Card(
                  margin: const EdgeInsets.all(10),
                  child: ListTile(
                    tileColor: Colors.blueGrey,
                    title: Text(documentSnapshot['name'], style: GoogleFonts.abel(fontSize: 20)),
                    subtitle: Text(documentSnapshot['price'].toString()+'\n'+documentSnapshot['size']+'\n' +documentSnapshot['model'],style: GoogleFonts.abel(fontSize: 20) ),
                    trailing: SizedBox(
                      width: 100,
                      child: Row(
                        children: [
                          // Press this button to edit a single product
                          IconButton(
                              icon: const Icon(Icons.edit),
                              onPressed: () =>
                                  _createOrUpdate(documentSnapshot)),
                          // This icon button is used to delete a single product
                          IconButton(
                              icon: const Icon(Icons.delete),
                              onPressed: () =>
                                  _deleteProduct(documentSnapshot.id)),
                        ],
                      ),
                    ),
                  ),
                );
              },
            );
          }

          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.indigoAccent,
              ),
              child: image(
                assetImage: AssetImage("assets/images/pp.png"),
              ),
            ),
            ListTile(
              leading: Icon(Icons.people_outlined),
              iconColor: Colors.blueAccent,
              title: const Text('About'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => About()),
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.stacked_bar_chart),
              iconColor: Colors.indigoAccent,
              title: const Text('Chart'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => PieChartSample3()),
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.accessibility_new_rounded),
              iconColor: Colors.indigoAccent,
              title: const Text('Profile'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Pooyu()),
                );
              },
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _createOrUpdate(),
        child: const Icon(Icons.add),
      ),
    );
  }
}
