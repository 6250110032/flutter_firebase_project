import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_firebase_auth/login_google.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'authentication.dart';
import 'login.dart';

class Pooyu extends StatelessWidget {
  FirebaseAuth _auth = FirebaseAuth.instance;
  get user => _auth.currentUser;

  final GoogleSignInAccount userG = googleSignIn.currentUser;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.indigo,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.logout,
              color: Colors.white,
              size: 30,
            ),
            onPressed: () async {
              await handleSignOut().whenComplete(() => Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => (Login())),
                  ));
              AuthenticationHelper()
                  .signOut()
                  .then((_) => Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(builder: (contex) => Login()),
                      ));
            },
          )
        ],
        systemOverlayStyle:
            SystemUiOverlayStyle(statusBarColor: Colors.blueAccent),
        title: Text(
          "Profile",
          style: GoogleFonts.allertaStencil(fontSize: 20),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 175),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 50.0),
          height: MediaQuery.of(context).size.height * 0.35,
          child: Card(
            color: Colors.blueGrey,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.0),
            ),
            elevation: 8,
            child: Container(
              child: Center(
                child: Container(
                  child: Text(
                    user?.email ?? userG.email,
                    style: GoogleFonts.prompt(fontSize: 30, color: Colors.white),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
